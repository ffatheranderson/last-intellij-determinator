package home.sapronov.pasha.intellij.last.version.determinator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Hello world!
 */
public class App {
	public static void main(String[] args) throws IOException {
		if (args.length == 0)
			throw new RuntimeException("The program expects you to pass the location of folder with generated cmd files as an input.");

		final String pathToDirWithGeneratedCMDFiles = args[0];

		Set<Path> allCMDs = Files.list(new File(pathToDirWithGeneratedCMDFiles).toPath()).filter(path -> path.toString().endsWith(".cmd")).collect(Collectors.toSet());

		Set<String> pathsToIntellijIdeasExeFiles = allCMDs.stream().map(p -> {
			try (Scanner s = new Scanner(p.toFile())) {
				while (s.hasNext()) {
					String line = s.nextLine();
					if (line.contains("idea64.exe"))
						return line.split(" ")[3];
				}
			} catch (FileNotFoundException e) {
				throw new RuntimeException(e);
			}

			return "";
		}).collect(Collectors.toSet());

//		System.out.println("All versions: ");
//		pathsToIntellijIdeasExeFiles.stream().forEach(s -> {
//			System.out.println(s);
//		});

		Optional<String> lastIntellijIdea = pathsToIntellijIdeasExeFiles.stream().max(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				String[] s1 = o1.split("\\\\");
				String[] s2 = o2.split("\\\\");

				String toCompare1 = s1[s1.length - 3];
				String toCompare2 = s2[s2.length - 3];

				String[] version1 = toCompare1.split("\\.");
				String[] version2 = toCompare2.split("\\.");


				for (int i = 0; i < version1.length; i++) {
					int v1 = Integer.valueOf(version1[i]);
					int v2 = Integer.valueOf(version2[i]);

					int result = Integer.compare(v1, v2);
					if (result != 0)
						return result;
				}

				return 0;
			}
		});

		if (lastIntellijIdea.isEmpty())
			throw new RuntimeException("No versions available.");


		lastIntellijIdea.map(s -> s.replace('\\', '/'))
				.map(s -> s.replace(":", ""))
				.map(s -> {
					char diskLetter = s.charAt(0);
					return Character.toLowerCase(diskLetter) + s.substring(1);
				})
				.map(s -> "/mnt/" + s)
				.ifPresent(System.out::println);

	}

}
